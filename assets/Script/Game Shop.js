
cc.Class({
    extends: cc.Component,

    properties: {
        exit: {
            default: null,
            type: cc.Node
        },

        buyBg: {
            default: null,
            type: cc.Node,
        },

        button: {
            default: null,
            type: cc.Node
        },

        title: {
            default: null,
            type: cc.Node,
        },

        titleBg: {
            default: null,
            type: cc.Node
        },

        gold: {
            default: null,
            type: cc.Node,
        },

        goldScore: {
            default: null,
            type: cc.Node,
        },

        avatar: {
            default: null,
            type: cc.Node,
        },

        radioButton: {
            default: [],
            type: cc.Toggle
        },

        itemPriceLable: {
            default: [],
            type: cc.Label
        },

        audioWrong: {
            default: null,
            url: cc.AudioClip
        },

        audioCoin2: {
            default: null,
            url: cc.AudioClip
        },

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.lastSelected = [];
        this.boughtList = [];
        this.activeflag = false;
        this._goldScore = 0;
        this.itemPrice = [0, 500, 700, 800, 1000, 2000];
    },

    start() { },

    onEnable() {
        cc.log(this.itemPriceLable[1].node.name);
        this.goldScore.getComponent(cc.Label).string = 0;
        if (cc.sys.localStorage.getItem("goldScore") != null) {
            this.goldScore.getComponent(cc.Label).string = cc.sys.localStorage.getItem("goldScore");
            this._goldScore = Number(cc.sys.localStorage.getItem("goldScore"));
        }

        this.lastSelected = this.json2Array(JSON.parse(cc.sys.localStorage.getItem("lastSelected")));
        this.boughtList = this.json2Array(JSON.parse(cc.sys.localStorage.getItem("boughtList")));
        // cc.log (JSON.parse(cc.sys.localStorage.getItem("boughtList")).array);
        for (var i = 0; i < 6; i++) {
            if (this.boughtList[i] == 1) {
                if(i > 0){
                    this.setItemPriceLable(i);
                }
                if (this.lastSelected[i] == 1) {
                    this.radioButton[i].check();
                }

            }
        }

        this.reset();
        this.titleBg.runAction(cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1)));
        this.buyBg.runAction(cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1)));
        this.exit.runAction(cc.sequence(cc.delayTime(0.3), cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1))));
        this.title.runAction(cc.sequence(cc.delayTime(0.5), cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1))));
        this.gold.runAction(cc.sequence(cc.delayTime(0.7), cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1))));
        this.goldScore.runAction(cc.sequence(cc.delayTime(0.9), cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1))));
        this.avatar.runAction(cc.sequence(cc.delayTime(1.1), cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1))));
        this.button.runAction(cc.sequence(cc.delayTime(0.5), cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1))));


    },

    reset() {
        this.exit.x = -719;
        this.title.x = -1000;
        this.titleBg.x = -1000;
        this.gold.x = -1193;
        this.goldScore.x = -1139;
        this.avatar.x = -1000;
        this.button.x = -1000;
        this.buyBg.x = -1000;
    },

    radioButtonClicked(toggle) {
        var index = this.radioButton.indexOf(toggle);
        this.setLastSelected(index);
        cc.sys.localStorage.setItem("lastSelected", JSON.stringify(this.array2Json(this.lastSelected)));
        cc.log(cc.sys.localStorage.getItem("lastSelected"));
    },



    setBoughtList(i) {
        this.boughtList[i] = 1;
        cc.sys.localStorage.setItem("boughtList", JSON.stringify(this.array2Json(this.boughtList)));
    },

    setLastSelected(i) {
        this.lastSelected = [0, 0, 0, 0, 0, 0];
        this.lastSelected[i] = 1;
    },

    checkBoughtItem(i) {
        if (this.boughtList[i] == 1) {
            return true;
        }
        return false;
    },

    setItemPriceLable(index) {
        this.itemPriceLable[index].string = "GOT";
        this.itemPriceLable[index].node.color = cc.Color.YELLOW;
    },

    onBuyTouched() {
        // var json = this.json2Array(JSON.parse(cc.sys.localStorage.getItem("lastSelected")));
        var lastSelected = this.json2Array(JSON.parse(cc.sys.localStorage.getItem("lastSelected")));
        var index = 0;
        for (var i = 0; i < 6; i++) {
            if (lastSelected[i] == 1) index = i;
        }
        if (this.checkBoughtItem(index) == false && this._goldScore >= Number(this.itemPrice[index])) {
            this.setBoughtList(index);
            this.setItemPriceLable(index);
            if ( index > 0) cc.audioEngine.play(this.audioCoin2, false, 1);
        } else {
            if ( index > 0) cc.audioEngine.play(this.audioWrong, false, 1);
        }
    },

    json2Array(json){
        var array = [json._0,json._1,json._2,json._3,json._4,json._5];
        return array;
    },

    array2Json(array){
        var json = {
            _0 : array[0],
            _1 : array[1],
            _2 : array[2],
            _3 : array[3],
            _4 : array[4],
            _5 : array[5],
        }
        return json;
    },
    // update (dt) { },
});
