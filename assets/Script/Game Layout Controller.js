cc.Class({
    extends: cc.Component,

    properties: {
        gameMenuLayout: {
            default: null,
            type: cc.Node
        },

        gameScoreLayout: {
            default: null,
            type: cc.Node
        },

        gameShopLayout: {
            default: null,
            type: cc.Node
        },

        gamePlayLayout: {
            default: null,
            type: cc.Node
        },

        audioBackground: {
            default: null,
            url: cc.AudioClip
        },

        audioButtonTouched: {
            default: null,
            url: cc.AudioClip
        },

        radioButton: {
            default: [],
            type: cc.Toggle
        }

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.gameOverTime = 0;
        this.currentMts = 0;
        // cc.sys.localStorage.setItem("bestMts", 0);
        // cc.sys.localStorage.setItem("goldScore", 0);
        // cc.sys.localStorage.setItem("bestManPos", 0);

        var lastSelected = {
            _0 : 1,
            _1 : 0,
            _2 : 0,
            _3 : 0,
            _4 : 0,
            _5 : 0, 
        };
        var boughtList = {
            _0 : 1,
            _1 : 0,
            _2 : 0,
            _3 : 0,
            _4 : 0,
            _5 : 0, 
        };
        // cc.sys.localStorage.removeItem("boughtList");
        // cc.sys.localStorage.removeItem("lastSelected");
        if (cc.sys.localStorage.getItem("lastSelected") == null) {
            cc.sys.localStorage.setItem("lastSelected",JSON.stringify(lastSelected));
            // cc.sys.localStorage.setItem("lastSelected", lastSelected);
        }
        if (cc.sys.localStorage.getItem("boughtList") == null) {
            cc.sys.localStorage.setItem("boughtList",JSON.stringify(boughtList));
        }

        this.bestMts = cc.sys.localStorage.getItem("bestMts");
        if (this.bestMts == null) {
            cc.sys.localStorage.setItem("bestMts", 0);
        }
        this.goldScore = cc.sys.localStorage.getItem("goldScore");
        if (this.goldScore == null) {
            cc.sys.localStorage.setItem("goldScore", 0);
        }
        this.bestManPos = cc.sys.localStorage.getItem("bestManPos");
        if (this.bestManPos == null) {
            cc.sys.localStorage.setItem("bestManPos", 0);
        }

        this.audioBackgroundId = cc.audioEngine.play(this.audioBackground, true, 1);
        // cc.audioEngine.setVolume(this.audioBackgroundId, 0);
    },

    start() {

    },

    onBtnNewGameTouched() {

        cc.audioEngine.pause(this.audioBackgroundId);
        var fade = cc.fadeOut(0.5);
        var temp = this;
        var func = function () {
            temp.gameMenuLayout.active = false;
            temp.gamePlayLayout.active = true;
            cc.audioEngine.resume(temp.audioBackgroundId);
        };
        var seq = cc.sequence(cc.delayTime(0.3), fade, cc.callFunc(func));
        this.gameMenuLayout.runAction(seq);
    },

    onBtnScoreTouched() {
        cc.audioEngine.play(this.audioButtonTouched, false, 1);
        var temp = this;
        cc.audioEngine.pause(temp.audioBackgroundId);
        var func = function () {
            temp.gameMenuLayout.active = false;
            temp.gameScoreLayout.active = true;
            cc.audioEngine.resume(temp.audioBackgroundId);
        };
        var seq = cc.sequence(fade, cc.callFunc(func));
        this.gameMenuLayout.runAction(seq);
    },

    onBtnShopTouched() {
        cc.audioEngine.pause(this.audioBackgroundId);

        var fade = cc.fadeOut(0.5);
        var temp = this;
        var func = function () {
            temp.gameMenuLayout.active = false;
            temp.gameShopLayout.active = true;
            cc.audioEngine.resume(temp.audioBackgroundId);
        };
        var seq = cc.sequence(fade, cc.callFunc(func));
        this.gameMenuLayout.runAction(seq);
    },

    onTryAgainTouched() {
        cc.audioEngine.pause(this.audioBackgroundId);
        cc.audioEngine.play(this.audioButtonTouched, false, 1);
        cc.audioEngine.resume(this.audioBackgroundId);
        this.gameScoreLayout.active = false;
        this.gamePlayLayout.active = true;
    },

    onBtnExitTouched() {
        cc.audioEngine.pause(this.audioBackgroundId);
        cc.audioEngine.play(this.audioButtonTouched, false, 1);
        this.gamePlayLayout.active = false;
        this.gameScoreLayout.active = false;
        this.gameShopLayout.active = false;
        this.gameMenuLayout.active = true;
        cc.audioEngine.resume(this.audioBackgroundId);
        this.gameMenuLayout.runAction(cc.fadeTo(0.3, 255));
    },

    onPlayTouched() {
        var lastSelected = this.json2Array(JSON.parse(cc.sys.localStorage.getItem("lastSelected")));
        var boughtList = this.json2Array(JSON.parse(cc.sys.localStorage.getItem("boughtList")));
        var index = 0;
        for (var i = 0; i < 6; i++) {
            if (lastSelected[i] == 1) index = i;
        }
        if (boughtList[index] == 1) {
            cc.audioEngine.pause(this.audioBackgroundId);
            cc.audioEngine.play(this.audioButtonTouched, false, 1);
            cc.audioEngine.resume(this.audioBackgroundId);
            this.gameShopLayout.active = false;
            this.gamePlayLayout.active = true;
        }
    },

    json2Array(json){
        var array = [json._0,json._1,json._2,json._3,json._4,json._5];
        return array;
    },

    array2Json(array){
        var json = {
            _0 : array[0],
            _1 : array[1],
            _2 : array[2],
            _3 : array[3],
            _4 : array[4],
            _5 : array[5],
        }
        return json;
    },
    update(dt) {

        if (this.gamePlayLayout.getComponent("Game Play").manController.isAlive == false
            && this.gameScoreLayout.active == false && this.gamePlayLayout.active == true) {
            this.gameOverTime += dt;
            this.currentMts = this.gamePlayLayout.getComponent("Game Play").currentMts;
            cc.audioEngine.pause(this.audioBackgroundId);
            cc.sys.localStorage.setItem("goldScore", this.gamePlayLayout.getComponent("Game Play").goldScore.string);
            if (this.gameOverTime >= 2) {
                this.gamePlayLayout.active = false;
                this.gameScoreLayout.active = true;
                this.gameScoreLayout.getComponent("Game Score").setCurrentMtsScore(this.currentMts);
                this.gameOverTime = 0;
                cc.audioEngine.resume(this.audioBackgroundId);
            }
        }
    },
});
