const State = cc.Enum({
    Ready: 0,
    Rise: 1,
    FreeFall: 2,
    Dead: 3,
});
var GamePlay = require("Game Play");
const GameConfig = require('GameConfig');
cc.Class({
    extends: cc.Component,

    statics: {
        State: State
    },

    properties: {
        isAlive: true,
        firstMove: true,
        isTurnLeft: true,
        collideLeftWall: false,
        collideRightWall: false,
        currentSpeed: 0,
        goldScore: 0,
        goldStep: 1,
        state: {
            default: State.Ready,
            type: State
        },
        overlay: {
            type: cc.Node,
            default: null
        },

        tapToStart: {
            default: null,
            type: cc.Node
        },

        audioJump: {
            default: null,
            url: cc.AudioClip
        },

        audioCollide: {
            default: null,
            url: cc.AudioClip
        },

        audioDead: {
            default: null,
            url: cc.AudioClip
        },

        audioGameOver1: {
            default: null,
            url: cc.AudioClip
        },

        audioGameOver2: {
            default: null,
            url: cc.AudioClip
        },

        audioCoin: {
            default: null,
            url: cc.AudioClip
        },

        audioCoin2: {
            default: null,
            url: cc.AudioClip
        },

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        cc.director.getCollisionManager().enabled = true;
        cc.director.getCollisionManager().enabledDebugDraw = false;
        this.lastSelected = [];
        this.boughtList = [];
        this.goldScore = 0;
        this.lastSelected = 0;
        this.playedAnimation = false;
        if (cc.sys.localStorage.getItem("goldScore") != null) {
            this.goldScore = Number(cc.sys.localStorage.getItem("goldScore"));
        }
        this.reset();
        /////////////////////////////////////
        this.caveman = cc.textureCache.addImage(cc.url.raw("resources/caveman.png"));
        this.newton = cc.textureCache.addImage(cc.url.raw("resources/newton.png"));
        this.hulk = cc.textureCache.addImage(cc.url.raw("resources/hulk.png"));
        this.snail = cc.textureCache.addImage(cc.url.raw("resources/snail.png"));
        this.pirate = cc.textureCache.addImage(cc.url.raw("resources/pirate.png"));
        this.flash = cc.textureCache.addImage(cc.url.raw("resources/flash.png"));
        ////////////////////////////////////
    },

    start() {
        var _man = this.node;
        var self = this;
        this.tapToStart.on(cc.Node.EventType.TOUCH_START, function (event) {
            self.overlay.x = 0;
            self.rise();
            
            self.firstTap = false;
            var func = function () {
                self.tapToStart.active = false;
            }
            self.tapToStart.runAction(cc.sequence(cc.moveBy(0.5, cc.p(-1000, 0)), cc.callFunc(func)));
        });
        // if (!this.firstTap){
        this.overlay.on(cc.Node.EventType.TOUCH_START, function (event) {
            self.rise();
        }, this.overlay);
        // }
    },



    onCollisionEnter(other, self) {
        if (other.node.name == "wall2 left") {
            this.collideLeftWall = true;
            this.isTurnLeft = false;
            cc.audioEngine.play(this.audioCollide, false, 1);
        }
        if (other.node.name == "wall2 right") {
            this.collideRightWall = true;
            this.isTurnLeft = true;
            cc.audioEngine.play(this.audioCollide, false, 1);
        }

        if (other.node.name == "ceiling") {
            this.state = State.FreeFall;
            this.time_jump = 0;
            this.lastPos = this.node.y;
        }

        if (other.node.name == "Spike") {
            this.isAlive = false;
            this.overlay.x = -1000;
        }

        if (other.node.name == "Last record") {
            other.node.y -= 1000;
        }

        if (other.node.name == "Gold") {
            other.node.x = -1000;
            this.goldScore += this.goldStep;
            if (Math.floor(Math.random() * 6) + 1 % 2 == 0){
                cc.audioEngine.play(this.audioCoin,false,1);
            }else cc.audioEngine.play(this.audioCoin2,false,1);
        }
    },

    onCollisionExit(other, self) {
        if (other.node.name == "wall2 left") {
            this.collideLeftWall = false;
        }
        if (other.node.name == "wall2 right") {
            this.collideRightWall = false;
        }
    },

    update(dt) {
        // if (!this.firstTap){

        if (this.isAlive && !this.firstTap) {
            this.updateXPosition(dt);
            if (!this.isGrounded) {
                this.updateYPosition(dt);
            }
        }
        if (this.isAlive == false) {
            if (!this.isGrounded) {
                this.updateYPosition(dt);
            }
            if (this.playedAnimation == false) {
                this.node.getComponent(cc.Animation).play();
                this.playedAnimation = true;
                cc.audioEngine.play(this.audioDead, false, 1);
                if (dt * 1000 % 2 == 0) {
                    cc.audioEngine.play(this.audioGameOver1, false, 0.8);
                } else cc.audioEngine.play(this.audioGameOver2, false, 0.8);
            }
        }
    },

    updateState() {
        if (this.currentSpeed <= 0) {
            this.state = State.FreeFall;
        }
    },

    updateXPosition(dt) {
        if (this.firstMove == true) {
            this.node.x -= dt * GameConfig.MAN_SPEED;
            if (this.collideLeftWall == true) {
                this.firstMove = false;
            }
        } else {
            if (this.collideLeftWall == true || this.isTurnLeft == false) {
                if (this.collideLeftWall == true) {
                    this.node.runAction(cc.flipX(true));
                }
                this.node.x += dt * GameConfig.MAN_SPEED;
            }
            if (this.collideRightWall == true || this.isTurnLeft == true) {
                if (this.collideRightWall == true) {
                    this.node.runAction(cc.flipX(false));
                }
                this.node.x -= dt * GameConfig.MAN_SPEED;
            }
        }
    },

    updateYPosition(dt) {
        var rising = this.state === State.Rise || this.state === State.FreeFall;
        if (rising) {
            this.time_jump += dt;

            if (this.state === State.Rise) {
                this.node.y = this.lastPos + GameConfig.JUMP_INIT_SPEED * this.time_jump -
                    0.5 * GameConfig.GRAVITY * this.time_jump * this.time_jump;

            }
            if (this.state === State.FreeFall) {
                this.node.y = this.lastPos - 0.5 * GameConfig.GRAVITY * this.time_jump * this.time_jump;
            }
            if (this.node.y <= GameConfig.BOTTOM_POSITION) {
                this.isGrounded = true;
                this.node.y = GameConfig.BOTTOM_POSITION;
            }
        }
    },

    rise() {
        cc.audioEngine.play(this.audioJump, false, 1);
        this.node.getComponent(cc.Animation).play("Man Jump");
        this.lastPos = this.node.y;
        this.state = State.Rise;
        this.isGrounded = false;
        this.time_jump = 0;
    },

    setActorSprite(){
        var a;
        this.lastSelected = this.json2Array(JSON.parse(cc.sys.localStorage.getItem("lastSelected")));
        this.boughtList = this.json2Array(JSON.parse(cc.sys.localStorage.getItem("boughtList")));

        for ( var i = 0; i < 6; i++){
            if (this.lastSelected[i] == 1 && this.boughtList[i] == 1){
                a = i;
            }
        }

        switch(a){
            case 0: {
                this.node.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(this.caveman);
                break;
            }
            case 1: {
                this.node.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(this.newton);
                break;
            }
            case 2: {
                this.node.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(this.hulk);
                break;
            }
            case 3: {
                this.node.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(this.snail);
                break;
            }
            case 4: {
                this.node.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(this.pirate);
                break;
            }
            case 5: {
                this.node.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(this.flash);
                break;
            }
        }
    },

    reset() {
        this.overlay.x = -1000;
        this.playedAnimation = false;
        this.isAlive = true;
        this.firstMove = true;
        this.isTurnLeft = true;
        this.collideLeftWall = false;
        this.collideRightWall = false;
        this.currentSpeed = 0;
        this.isGrounded = false;
        this.time_jump = 0;
        this.lastPos = 0;
        this.firstTap = true;
        this.node.x = 0;
        this.node.y = 0;
        this.node.runAction(cc.flipX(false));
        this.tapToStart.active = true;
        this.tapToStart.x = 0;
        this.setActorSprite();
    },

    json2Array(json){
        var array = [json._0,json._1,json._2,json._3,json._4,json._5];
        return array;
    },

    array2Json(array){
        var json = {
            _0 : array[0],
            _1 : array[1],
            _2 : array[2],
            _3 : array[3],
            _4 : array[4],
            _5 : array[5],
        }
        return json;
    },
});
