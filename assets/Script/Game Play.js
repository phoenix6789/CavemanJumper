const GameConfig = require('GameConfig');
var ManController = require('Man Controller');

const SpikeType = cc.Enum({
    LSP1: 1,
    LSP2: 2,
    LSP3: 3,
    RSP1: 4,
    RSP2: 5,
    RSP3: 6,
});

cc.Class({
    extends: cc.Component,
    statics: {
        SpikeType: SpikeType
    },
    properties: {

        man: {
            type: cc.Node,
            default: null
        },
        wallLeft: {
            type: cc.Node,
            default: null
        },
        wallRight: {
            type: cc.Node,
            default: null
        },
        leftSpikes: {
            default: null,
            type: cc.Node
        },

        rightSpikes: {
            default: null,
            type: cc.Node
        },

        lSpike1: {
            default: null,
            type: cc.Prefab
        },
        lSpike2: {
            default: null,
            type: cc.Prefab
        },
        lSpike3: {
            default: null,
            type: cc.Prefab
        },
        rSpike1: {
            default: null,
            type: cc.Prefab
        },
        rSpike2: {
            default: null,
            type: cc.Prefab
        },
        rSpike3: {
            default: null,
            type: cc.Prefab
        },

        goldPrefab: {
            default: null,
            type: cc.Prefab
        },

        lastRecordPrefab: {
            default: null,
            type: cc.Prefab
        },

        smoke: {
            default: null,
            type: cc.Prefab,
        },

        goldScore: {
            default: null,
            type: cc.Label,
        },

        milestone: {
            default: null,
            type: cc.Node,
        },

        mts: {
            default: null,
            type: cc.Label,
        },

        overlay: {
            default: null,
            type: cc.Node
        },

        manController: ManController,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.gameSpeed = GameConfig.GAME_SPEED;
        this.bestManPos = 0;
        this.currentMts = 0;
        this.LSPool1 = new cc.NodePool();
        this.LSPool2 = new cc.NodePool();
        this.LSPool3 = new cc.NodePool();
        this.RSPool1 = new cc.NodePool();
        this.RSPool2 = new cc.NodePool();
        this.RSPool3 = new cc.NodePool();
        this.goldPool = new cc.NodePool();
        this.smokePool = new cc.NodePool();

        this.reset();
        this.lr = cc.instantiate(this.lastRecordPrefab);
        this.leftSpikes.addChild(this.lr);
    },

    start() {
        var self = this;
        this.overlay.on(cc.Node.EventType.TOUCH_START, function (event) {
            var pos = cc.p(self.man.getPosition());
            var s = self.smokePool.get();
            self.node.addChild(s);
            s.setPosition(pos);
            var anim = s.getComponent(cc.Animation);
            var animState = anim.getAnimationState("smoke");
            animState.wrapMode = cc.WrapMode.Normal;
            anim.on('finished', function (event) {
                if (event.currentTarget == animState) {
                    self.smokePool.put(s);
                }
            });
            anim.play("smoke");
        }, this.overlay);
    },

    onEnable() {
       
        if (cc.sys.localStorage.getItem("bestManPos") != null) {
            this.bestManPos = Number(cc.sys.localStorage.getItem("bestManPos"));
        }
        this.reset();
        this.leftSpikes.addChild(this.lr);
        this.lr.x = 0;
        this.lr.y = cc.sys.localStorage.getItem("bestManPos") == null ? 0
            : Number(cc.sys.localStorage.getItem("bestManPos"));
        cc.log(this.lr.y);
        this.addFirstSpike();
    },

    update(dt) {
        // cc.log (this.leftSpikes.convertToNodeSpace(this.milestone.getPosition()).y);
        this.goldScore.string = this.manController.goldScore;
        if (this.manController.isAlive && !this.manController.firstTap) {
            this.leftSpikes.y -= this.gameSpeed;
            this.rightSpikes.y -= this.gameSpeed;
            this.recycleLeftSpike();
            this.recycleRightSpike();
            this.recycleLeftGold();
            this.recycleRightGold();
        }

        if (this.leftSpikes.y % 501 == 0 || this.leftSpikes.y % 500 == 0 || this.leftSpikes.y % 504 == 0) {
            // cc.log(this.leftSpikes.y);
        }
        if ((this.leftSpikes.convertToNodeSpace(this.node.convertToWorldSpace(
            this.milestone.getPosition())).y) / 100 >= 0) {
            this.currentMts = Math.floor((this.leftSpikes.convertToNodeSpace(this.node.convertToWorldSpace(
                this.milestone.getPosition())).y) / 100);
            this.mts.string = this.currentMts + " mts";

            if (this.currentMts == (40 + this.manController.goldStep * 40)) {
                this.manController.goldStep++;
                this.gameSpeed++;
            }
        } else this.mts.string = 0 + " mts";

        if (this.bestManPos <= this.leftSpikes.convertToNodeSpace(
            this.node.convertToWorldSpace(this.man.getPosition())).y) {
            this.bestManPos = this.leftSpikes.convertToNodeSpace(
                this.node.convertToWorldSpace(this.man.getPosition())).y;
            cc.sys.localStorage.setItem("bestManPos", this.bestManPos);
        }
    },

    /////////////////////////////////////////////////////////    

    recycleLeftSpike() {
        var ls = this.listLeftSpikes[this.listLeftSpikes.length - 7];
        if (this.leftSpikes.convertToWorldSpace(ls.getPosition()).y < 0) {
            this.putLeftSpikeToPool(this.listLeftSpikes.length - 7);
            this.addNewSpikeToLeftWall(this.listLeftSpikes[this.listLeftSpikes.length - 1])
        }
    },

    recycleLeftGold() {
        var g = this.listLeftGolds[this.listLeftGolds.length - 7];
        if (this.leftSpikes.convertToWorldSpace(g.getPosition()).y < 0) {
            this.goldPool.put(g);
            this.addNewGoldToLeftWall(this.listLeftGolds[this.listLeftGolds.length - 1]);
        }
    },

    addNewSpikeToLeftWall(preSpike) {
        var lType = Math.floor(Math.random() * 3 + 1);
        var ls = this.getSpikeFromPool(lType);
        ls.x = 0;
        ls.y = preSpike.y + this.randomDistance(this.mode);
        if (this.randomNumber(11, 1) > 3) this.leftSpikes.addChild(ls);
        this.listLeftSpikes.push(ls);
    },

    addNewGoldToLeftWall(preGold) {

        var g = null;
        if (this.goldPool.size() > 0) {
            g = this.goldPool.get();
        } else {
            g = cc.instantiate(this.goldPrefab);
        }
        g.x = 27;
        g.y = preGold.y + this.randomNumber(1000, 600);
        this.leftSpikes.addChild(g);
        this.listLeftGolds.push(g);
    },

    putLeftSpikeToPool(pos) {
        var spike = this.listLeftSpikes[pos];
        switch (spike.type) {
            case SpikeType.LSP1: {
                this.LSPool1.put(spike);
                break;
            }
            case SpikeType.LSP2: {
                this.LSPool2.put(spike);
                break;
            }
            case SpikeType.LSP3: {
                this.LSPool3.put(spike);
                break;
            }
        }
    },

    ////////////////////////////////////////////

    recycleRightSpike() {
        var rs = this.listRightSpikes[this.listRightSpikes.length - 7];
        if (this.leftSpikes.convertToWorldSpace(rs.getPosition()).y < 0) {
            this.putRightSpikeToPool(this.listRightSpikes.length - 7);
            this.addNewSpikeToRightWall(this.listRightSpikes[this.listRightSpikes.length - 1])
        }
    },

    recycleRightGold() {
        var g = this.listRightGolds[this.listRightGolds.length - 7];
        if (this.rightSpikes.convertToWorldSpace(g.getPosition()).y < 0) {
            this.goldPool.put(g);
            this.addNewGoldToRightWall(this.listRightGolds[this.listRightGolds.length - 1]);
        }
    },

    addNewSpikeToRightWall(preSpike) {
        var rType = Math.floor(Math.random() * 3 + 1) + 3;
        var rs = this.getSpikeFromPool(rType);
        rs.x = 0;
        rs.y = preSpike.y + this.randomDistance(this.mode);
        if (this.randomNumber(11, 1) > 3) this.rightSpikes.addChild(rs);
        this.listRightSpikes.push(rs);
    },

    addNewGoldToRightWall(preGold) {
        var g = null;
        if (this.goldPool.size() > 0) {
            g = this.goldPool.get();
        } else {
            g = cc.instantiate(this.goldPrefab);
        }
        g.x = - 27;
        g.y = preGold.y + this.randomNumber(1000, 600);
        this.rightSpikes.addChild(g);
        this.listRightGolds.push(g);
    },

    putRightSpikeToPool(pos) {
        var spike = this.listRightSpikes[pos];
        switch (spike.type) {
            case SpikeType.RSP1: {
                this.RSPool1.put(spike);
                break;
            }
            case SpikeType.RSP2: {
                this.RSPool2.put(spike);
                break;
            }
            case SpikeType.RSP3: {
                this.RSPool3.put(spike);
                break;
            }
        }
    },

    /////////////////////////////////////////////    
    createPools() {
        for (var i = 0; i < 6; i++) {
            var l1 = cc.instantiate(this.lSpike1);
            l1.type = SpikeType.LSP1;
            var l2 = cc.instantiate(this.lSpike2);
            l2.type = SpikeType.LSP2;
            var l3 = cc.instantiate(this.lSpike3);
            l3.type = SpikeType.LSP3;
            var r1 = cc.instantiate(this.rSpike1);
            r1.type = SpikeType.RSP1;
            var r2 = cc.instantiate(this.rSpike2);
            r2.type = SpikeType.RSP2;
            var r3 = cc.instantiate(this.rSpike3);
            r3.type = SpikeType.RSP3;

            this.LSPool1.put(l1);
            this.LSPool2.put(l2);
            this.LSPool3.put(l3);
            this.RSPool1.put(r1);
            this.RSPool2.put(r2);
            this.RSPool3.put(r3);

            var g1 = cc.instantiate(this.goldPrefab);
            var g2 = cc.instantiate(this.goldPrefab);
            this.goldPool.put(g1);
            this.goldPool.put(g2);

            var s1 = cc.instantiate(this.smoke);
            var s2 = cc.instantiate(this.smoke);
            this.smokePool.put(s1);
            this.smokePool.put(s2);
        }
    },

    addFirstSpike() {
        var lType = Math.floor(Math.random() * 3 + 1);
        var rType = Math.floor(Math.random() * 3 + 1) + 3;

        var g1, g2;
        g1 = this.goldPool.get();
        g2 = this.goldPool.get();
        this.listLeftGolds.push(g1);
        this.listRightGolds.push(g2);
        this.leftSpikes.addChild(g1);
        this.rightSpikes.addChild(g2);
        g1.x = 27; g2.x = - 27;
        g1.y = GameConfig.FIRST_SPIKE_Y_POSITION - 55;
        g2.y = GameConfig.FIRST_SPIKE_Y_POSITION - 55;

        var ls = this.getSpikeFromPool(lType);
        ls.x = 0;
        ls.y = GameConfig.FIRST_SPIKE_Y_POSITION;
        this.leftSpikes.addChild(ls);
        this.listLeftSpikes.push(ls);

        var rs = this.getSpikeFromPool(4);
        rs.x = 0;
        rs.y = GameConfig.FIRST_SPIKE_Y_POSITION;
        if (rType % 2 != 0) {
            rs.active = false;
            g2.active = false;
        }
        this.rightSpikes.addChild(rs);
        this.listRightSpikes.push(rs);

        for (var i = 0; i < 6; i++) {
            this.addNewSpikeToLeftWall(this.listLeftSpikes[this.listLeftSpikes.length - 1]);
            this.addNewSpikeToRightWall(this.listRightSpikes[this.listRightSpikes.length - 1]);
            this.addNewGoldToLeftWall(this.listLeftGolds[this.listLeftGolds.length - 1]);
            this.addNewGoldToRightWall(this.listRightGolds[this.listRightGolds.length - 1]);
        }
    },

    checkPools() {
        if (this.LSPool1.size() > 0 && this.LSPool2.size() > 0 && this.LSPool3.size() > 0
            && this.RSPool1.size() > 0 && this.RSPool2.size() > 0 && this.RSPool3.size() > 0) {
        } else {
            this.createPools();
        }
    },

    getSpikeFromPool(type) {
        var s = null;
        this.checkPools();
        switch (type) {
            case SpikeType.LSP1: {
                s = this.LSPool1.get();
                break;
            }
            case SpikeType.LSP2: {
                s = this.LSPool2.get();
                break;
            }
            case SpikeType.LSP3: {
                s = this.LSPool3.get();
                break;
            }
            case SpikeType.RSP1: {
                s = this.RSPool1.get();
                break;
            }
            case SpikeType.RSP2: {
                s = this.RSPool2.get();
                break;
            }
            case SpikeType.RSP3: {
                s = this.RSPool3.get();
                break;
            }
        }
        return s;
    },

    randomDistance(mode) {
        var d = 0;
        var c = this.randomNumber(15, 0);
        if (c == 1) {
            d = this.randomNumber(60, GameConfig.MIN_DISTANCE);
        } else if (c > 9 && c <= 11) {
            d = this.randomNumber(450, 380);
        } else {
            d = this.randomNumber(GameConfig.MAX_DISTANCE, 500);
        }

        return d;

    },

    randomNumber(max, min) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    },

    reset() {
        this.manController.reset();

        this.mode = 1;
        this.first = true;
        this.listLeftSpikes = [];
        this.listRightSpikes = [];
        this.listLeftGolds = [];
        this.listRightGolds = [];
        this.leftSpikes.y = 0;
        this.leftSpikes.removeAllChildren();
        this.rightSpikes.y = 0;
        this.rightSpikes.removeAllChildren();

        this.LSPool1.clear();
        this.LSPool2.clear();
        this.LSPool3.clear();
        this.RSPool1.clear();
        this.RSPool2.clear();
        this.RSPool3.clear();
        this.goldPool.clear();
        this.gameSpeed = GameConfig.GAME_SPEED;
        this.createPools();
    },

    json2Array(json){
        var array = [json._0,json._1,json._2,json._3,json._4,json._5,];
        return array;
    },

    array2Json(array){
        var json = {
            _0 : array[0],
            _1 : array[1],
            _2 : array[2],
            _3 : array[3],
            _4 : array[4],
            _5 : array[5],
        }
        return json;
    },
});
