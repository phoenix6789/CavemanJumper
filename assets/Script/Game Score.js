

cc.Class({
    extends: cc.Component,

    properties: {
        exit: {
            default: null,
            type: cc.Node
        },

        tryAgain: {
            default: null,
            type: cc.Node
        },

        titleBg: {
            default: null,
            type: cc.Node
        },

        tryAgainBg: {
            default: null,
            type: cc.Node,
        },

        title: {
            default: null,
            type: cc.Node,
        },

        gold: {
            default: null,
            type: cc.Node,
        },

        goldScore: {
            default: null,
            type: cc.Node,
        },

        // goldScore:{
        //     default:null,
        //     type: cc.Label
        // },

        currentMts: {
            default: null,
            type: cc.Node,
        },

        yourBestDistance: {
            default: null,
            type: cc.Node,
        },

        bestMts: {
            default: null,
            type: cc.Node,
        },

        newRecord: {
            default: null,
            type: cc.Node,
        },

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.currentMtsScore = 0;
        this.bestMtsScore = 0;
        this.setmts = 0;
        this.flag = true;
        this.newRecord.runAction(cc.repeatForever(cc.sequence(cc.moveBy(1,cc.p(50,0)),cc.moveBy(1, cc.p(-50,0)))));
    },

    // start() {

    // },

    onEnable() {
        // cc.log(this.goldScore.getComponent(cc.Label).string);
       
        // cc.repeatForever(cc.sequence(cc.scaleTo(0.8), cc.scaleTo(1.2)));
        this.flag = true;
        this.goldScore.getComponent(cc.Label).string = 0;
        if (cc.sys.localStorage.getItem("goldScore") != null) {
            this.goldScore.getComponent(cc.Label).string = cc.sys.localStorage.getItem("goldScore");
        }
        if (cc.sys.localStorage.getItem("bestMts") != null) {
            this.bestMtsScore = cc.sys.localStorage.getItem("bestMts");
        }
        this.bestMts.getComponent(cc.Label).string = this.bestMtsScore + " mts";
        this.currentMts.getComponent(cc.Label).string = this.currentMtsScore + " mts";
        this.reset();
        this.titleBg.runAction(cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1)));
        this.tryAgainBg.runAction(cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1)));
        this.exit.runAction(cc.sequence(cc.delayTime(0.3), cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1))));
        this.title.runAction(cc.sequence(cc.delayTime(0.5), cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1))));
        this.gold.runAction(cc.sequence(cc.delayTime(0.7), cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1))));
        this.goldScore.runAction(cc.sequence(cc.delayTime(0.9), cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1))));
        this.currentMts.runAction(cc.sequence(cc.delayTime(1.1), cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1))));
        this.yourBestDistance.runAction(cc.sequence(cc.delayTime(1.3), cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1))));
        this.bestMts.runAction(cc.sequence(cc.delayTime(1.5), cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1))));
        this.tryAgain.runAction(cc.sequence(cc.delayTime(0.5), cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1))));
    },

    reset() {
        this.titleBg.x = -1000;
        this.tryAgainBg.x = -1000;
        this.exit.x = -719;
        this.title.x = -1000;
        this.gold.x = -1193;
        this.goldScore.x = -1139;
        this.currentMts.x = -1000;
        this.yourBestDistance.x = -1000;
        this.bestMts.x = -1000;
        this.newRecord.x = -1000;
        this.tryAgain.x = -1000;
        this.flag = true;
        this.setmts = 0;
    },

    setCurrentMtsScore(mts) {
        this.currentMtsScore = mts;
    },

    onDisable(){
        this.reset();
    },

    newRecordAction() {
        // this.tryAgain.runAction(cc.sequence(cc.delayTime(1.7), cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1))));
        if (this.newRecord.active == true) {
            this.newRecord.runAction(cc.sequence(cc.delayTime(2), cc.moveBy(1, cc.p(1000, 0)).easing(cc.easeElasticOut(1)),
                cc.repeatForever(cc.sequence(cc.scaleTo(0.8), cc.scaleTo(1.2)))));
        }
    },
    update(dt) {
        this.setmts += dt;
        // this.currentMtsScore = 500;
        var temp = this.currentMtsScore;
        // if (temp == 0) {
            this.currentMts.getComponent(cc.Label).string = 0 + " mts";
        // }
        if (temp != 0) {
            this.currentMts.getComponent(cc.Label).string = temp + " mts";
            if (this.bestMtsScore < temp) {
                this.bestMtsScore = temp;
                this.bestMts.getComponent(cc.Label).string = this.bestMtsScore + " mts";
                cc.sys.localStorage.setItem("bestMts", this.bestMtsScore);
                this.flag = false;
            }
            temp = 0;
        }

        if (this.flag == false && this.setmts >= 2){
            this.newRecord.x = 0;
            this.flag = true;
        }

        
    },
});
