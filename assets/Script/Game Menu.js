

cc.Class({
    extends: cc.Component,
    
    properties: {
        title: {
            default: null,
            type: cc.Node
        },

        menuBackground: {
            default: null,
            type: cc.Node
        },

        newGame: {
            default: null,
            type: cc.Node
        },

        score: {
            default: null,
            type: cc.Node
        },

        shop: {
            default: null,
            type: cc.Node
        },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    // start() {    },

    resetPosition() {
        this.title.y = 870;
        this.newGame.x = -750;
        this.score.x = -750;
        this.shop.x = -750;
        this.menuBackground.x = -800;
    },

    onEnable(){

        this.resetPosition();
        this.title.runAction(cc.moveBy(1, cc.p(0, -500)).easing(cc.easeBounceOut(1)));
        this.menuBackground.runAction(cc.sequence(cc.delayTime(0.3), cc.moveBy(1, cc.p(800, 0))
            .easing(cc.easeElasticOut(2))));
        this.newGame.runAction(cc.sequence(cc.delayTime(0.5), cc.moveBy(1, cc.p(800, 0))
            .easing(cc.easeElasticOut(2))));
        this.score.runAction(cc.sequence(cc.delayTime(0.7), cc.moveBy(1, cc.p(800, 0))
            .easing(cc.easeElasticOut(2))));
        this.shop.runAction(cc.sequence(cc.delayTime(0.9), cc.moveBy(1, cc.p(800, 0))
            .easing(cc.easeElasticOut(2))));
    }
    // update (dt) {},

});
